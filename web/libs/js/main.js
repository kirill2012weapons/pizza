$(document).ready(function () {

    datepicker();

    var $collectionContainer = $('.collection_container-images');
    $collectionContainer.manageCollection({
        'insert-method': 'append'
    });
    var collectionManager = $collectionContainer.manageCollection()[0];

    $(".imgAdd").click(function () {
        collectionManager.before('.imgAdd');
    });
    $(function () {
        $(document).on("change", ".uploadFile", function () {
            var uploadFile = $(this);
            var files = !!this.files ? this.files : [];
            if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support

            if (/^image/.test(files[0].type)) { // only image file
                var reader = new FileReader(); // instance of the FileReader
                reader.readAsDataURL(files[0]); // read the local file

                reader.onloadend = function () { // set image data as background of div
                    //alert(uploadFile.closest(".upimage").find('.imagePreview').length);
                    uploadFile.closest(".imgUp").find('.imagePreview').css("background-image", "url(" + this.result + ")");
                }
            }

        });
    });

});

function datepicker() {
    $('#date_picker').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true
    });
}