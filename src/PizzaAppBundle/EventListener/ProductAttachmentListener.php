<?php

namespace PizzaAppBundle\EventListener;


use Doctrine\ORM\Event\LifecycleEventArgs;
use PizzaAppBundle\Entity\ProductAttachment;
use PizzaAppBundle\Widgets\Image\FileWorker;

class ProductAttachmentListener
{
    /**
     * @var FileWorker
     */
    private $imageWorker;

    public function __construct($imageWorker)
    {
        $this->imageWorker = $imageWorker;
    }

    private function getImageWoker()
    {
        return $this->imageWorker;
    }

    public function postLoad(LifecycleEventArgs $args)
    {
//        if (!$args->getEntity() instanceof ProductAttachment) return;
        /**
         * @var $entityProductAttachment ProductAttachment
         */
//        $entityProductAttachment = $args->getEntity();
//        $entityProductAttachment->setFile(
//            $this->getImageWoker()->getFileObjectByName($entityProductAttachment->getFile())
//        );
    }
}