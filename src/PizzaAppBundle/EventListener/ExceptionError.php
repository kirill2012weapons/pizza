<?php

namespace PizzaAppBundle\EventListener;


use PizzaAppBundle\Exception\PizzaException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

class ExceptionError
{

    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        if ($event->getException() instanceof PizzaException) {
            $this->getLogger()
                ->error(
                    'Message: ' . $event->getException()->getMessage() . PHP_EOL .
                    'File: ' . $event->getException()->getFile() . PHP_EOL .
                    'Line: ' . $event->getException()->getLine() . PHP_EOL . PHP_EOL
                );
            $response = new RedirectResponse('/something-is-broken/');
            $event->setResponse($response);
        }
    }

    public function test()
    {
        $this->getLogger()
            ->error('asd');
    }

    /**
     * @return LoggerInterface
     */
    public function getLogger()
    {
        return $this->logger;
    }
}