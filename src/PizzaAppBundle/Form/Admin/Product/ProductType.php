<?php

namespace PizzaAppBundle\Form\Admin\Product;


use PizzaAppBundle\Entity\JobKind;
use PizzaAppBundle\Entity\Product;
use PizzaAppBundle\Form\Admin\ProductAttachment\ProductAttachmentType;
use PizzaAppBundle\Repository\JobKindRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('jobKind', EntityType::class, [
                'class'             => JobKind::class,
                'query_builder'     => function (JobKindRepository $repository) {
                    return $repository->getAllJobsDESCForFormAsObj();
                },
                'choice_value' => function (JobKind $entity = null) {
                    return $entity ? $entity->getId() : null;
                },
            ])
            ->add('title', TextType::class, [

            ])
            ->add('description', TextType::class, [

            ])
            ->add('productAttachments', CollectionType::class, [
                'entry_type' => ProductAttachmentType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'product_form';
    }

}