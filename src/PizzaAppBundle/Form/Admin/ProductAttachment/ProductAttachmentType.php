<?php

namespace PizzaAppBundle\Form\Admin\ProductAttachment;


use PizzaAppBundle\Entity\Product;
use PizzaAppBundle\Entity\ProductAttachment;
use PizzaAppBundle\Repository\ProductRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;
use Vich\UploaderBundle\Form\Type\VichFileType;

class ProductAttachmentType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', ChoiceType::class, [
                'choices'  => [
                    'Title Image'       => ProductAttachment::TITLE_IMAGE,
                    'Gallery Image'     => ProductAttachment::GALLERY_IMAGE
                ],
                    ])
            ->add('file', VichFileType::class , [
                'required' => false,
                'allow_delete' => true,
//                'constraints' => [
//                    new NotBlank([
//                        'message' => 'Add IMAGE'
//                    ]),
//                    new File([
//                        'maxSize' => '10M',
//                        'maxSizeMessage' => 'Not more then 10M',
//                        'mimeTypes' => ["image/jpeg", "image/png"],
//                        'mimeTypesMessage' => "Must be JPEG, JPG or PNG, bro.",
//                        'uploadNoFileErrorMessage' => "No File uploaded!"
//                    ])
//                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ProductAttachment::class,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'product_attachment_form';
    }

}