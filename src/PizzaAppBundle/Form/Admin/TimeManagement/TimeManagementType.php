<?php

namespace PizzaAppBundle\Form\Admin\TimeManagement;


use PizzaAppBundle\Entity\JobKind;
use PizzaAppBundle\Entity\TimeManagement;
use PizzaAppBundle\Entity\Workload;
use PizzaAppBundle\Repository\JobKindRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TimeManagementType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('jobKind', EntityType::class, [
                'class'             => JobKind::class,
                'query_builder'     => function (JobKindRepository $repository) {
                    return $repository->getAllJobsDESCForFormAsObj();
                },
                'choice_value' => function (JobKind $entity = null) {
                    return $entity ? $entity->getId() : null;
                },
            ])
            ->add('countCooks', TextType::class, [

            ])
            ->add('minutes', TextType::class, [

            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'default_class' => TimeManagement::class,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'time_management_form';
    }

}