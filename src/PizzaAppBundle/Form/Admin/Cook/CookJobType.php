<?php

namespace PizzaAppBundle\Form\Admin\Cook;


use PizzaAppBundle\Entity\Cook;
use PizzaAppBundle\Entity\JobKind;
use PizzaAppBundle\Repository\CookRepository;
use PizzaAppBundle\Repository\JobKindRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CookJobType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', IntegerType::class, [
                'disabled' => true,
            ])
            ->add('name', TextType::class, [
                'disabled' => true,
            ])
            ->add('jobKind', EntityType::class, [
                'class'         => JobKind::class,
                'empty_data'   => null,
                'query_builder' => function(JobKindRepository $repository) {
                    return $repository->getAllJobsDESCForFormAsObj();
                },
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'default_class' => Cook::class,
            ]);
    }

    public function getBlockPrefix()
    {
        return 'cook_job_type';
    }
}