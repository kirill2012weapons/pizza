<?php

namespace PizzaAppBundle\Form\Admin\Cook;


use PizzaAppBundle\Entity\Cook;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CookType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [

            ])
            ->add('surname', TextType::class, [

            ])
            ->add('age', TextType::class, [

            ])
            ->add('livingPlace', TextType::class, [

            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Cook::class,
        ]);
    }

    public function getBlockPrefix()
    {
        return "cook_form";
    }
}