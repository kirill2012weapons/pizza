<?php

namespace PizzaAppBundle\Validator\Constraints;


use Doctrine\ORM\EntityManager;
use PizzaAppBundle\Entity\Cook;
use PizzaAppBundle\Entity\Workload;
use PizzaAppBundle\Repository\WorkLoadRepository;
use Symfony\Component\Form\Form;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class IsCookCountExistInCurrentJobKindValidator extends ConstraintValidator
{

    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    public function validate($value, Constraint $constraint)
    {
        /**
         * @var $currentFormEntity Workload
         */
        $currentFormEntity = $this->context->getObject();
        /**
         * @var $repWorkLoadsRep WorkLoadRepository
         */
        $repWorkLoadsRep = $this->getEntityManager()
            ->getRepository(Workload::class);

        $res = [];
        if ($currentFormEntity->getJobKind()) {
            /**
             * @var $form Form
             */
            $date = $this->context->getRoot()->get('date')->getNormData();
            $res = $repWorkLoadsRep->getCookCountInCurrentJobKindAsArray($currentFormEntity->getJobKind()->getId(), $currentFormEntity->getCooksCount(), $date);
            dump($res);
            /**
             * @var $constraint IsCookCountExistInCurrentJobKind
             */
            if (!empty($res)) $this->context->addViolation($constraint->message, [
                '{{ value }}'       => $value,
                '{{ date }}'        => $date,
                '{{ kind }}'        => $currentFormEntity->getJobKind()->getId(),
            ]);
        }
    }

    public function getEntityManager()
    {
        return $this->em;
    }

}