<?php

namespace PizzaAppBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @package PizzaAppBundle\Validator\Constraints
 */
class IsCookCountExistInCurrentJobKind extends Constraint
{

    public $message = '{{ value }} Count Of cook in the {{ kind }} Job Kind on the date - {{ date }} is exist';

    public function validatedBy()
    {
        return 'is_cook_count_exist_in_current_job_kind.validator';
    }

}