<?php

namespace PizzaAppBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @package PizzaAppBundle\Validator\Constraints
 */
class IsCookCountPresentInJobKind extends Constraint
{

    public $message = '{{ value }} Count Of cook in the {{ kind }} is exist';

    public function validatedBy()
    {
        return 'is_cook_count_present_in_job_kind.validator';
    }

}