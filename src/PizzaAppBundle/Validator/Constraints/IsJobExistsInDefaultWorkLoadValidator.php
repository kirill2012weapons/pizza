<?php

namespace PizzaAppBundle\Validator\Constraints;


use Doctrine\ORM\EntityManager;
use PizzaAppBundle\Entity\WorkloadDefault;
use PizzaAppBundle\Repository\WorkLoadDefaultRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class IsJobExistsInDefaultWorkLoadValidator extends ConstraintValidator
{

    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    public function validate($value, Constraint $constraint)
    {
        /**
         * @var $currentFormEntity WorkloadDefault
         */
        $currentFormEntity = $this->context->getObject();
        dump($this);
        /**
         * @var $repWorkLoadsDefaultRep WorkLoadDefaultRepository
         */
        $repWorkLoadsDefaultRep = $this->getEntityManager()
            ->getRepository(WorkloadDefault::class);

        if (is_null($currentFormEntity->getJobKind())) return;

        $res = [];

        $res = $repWorkLoadsDefaultRep->getDefaultJobIdIfExists(
            $currentFormEntity->getJobKind()->getId()
        );

        /**
         * @var $constraint IsJobExistsInDefaultWorkLoad
         */
        if (!empty($res)) $this->context->addViolation($constraint->message);
    }

    public function getEntityManager()
    {
        return $this->em;
    }

}