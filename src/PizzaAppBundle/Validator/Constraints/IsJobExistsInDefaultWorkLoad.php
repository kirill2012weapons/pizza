<?php

namespace PizzaAppBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @package PizzaAppBundle\Validator\Constraints
 */
class IsJobExistsInDefaultWorkLoad extends Constraint
{

    public $message = 'Cannot create Default value for this Work Load. Couse is already isset.';

    public function validatedBy()
    {
        return 'is_job_exists_in_default_workLoad.validator';
    }

}