<?php

namespace PizzaAppBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @package PizzaAppBundle\Validator\Constraints
 */
class IsMinutesExistInJobKindWithCookCount extends Constraint
{

    public $message = '{{ minutes }} minutes in the {{ job }} with {{ cookCount }} is exist. Try another value or change this value.';

    public function validatedBy()
    {
        return 'is_minutes_exist_in_job_kind_with_cook_count.validator';
    }

}