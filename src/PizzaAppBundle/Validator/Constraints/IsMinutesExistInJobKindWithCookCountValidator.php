<?php

namespace PizzaAppBundle\Validator\Constraints;


use Doctrine\ORM\EntityManager;
use PizzaAppBundle\Entity\TimeManagement;
use PizzaAppBundle\Repository\TimeManagementRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class IsMinutesExistInJobKindWithCookCountValidator extends ConstraintValidator
{

    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    public function validate($value, Constraint $constraint)
    {
        /**
         * @var $currentFormEntity TimeManagement
         */
        $currentFormEntity = $this->context->getObject();
        /**
         * @var $timeManagementRep TimeManagementRepository
         */
        $timeManagementRep = $this->getEntityManager()
            ->getRepository(TimeManagement::class);

        $res = [];
        if ($currentFormEntity->getJobKind() && $currentFormEntity->getCountCooks() && $currentFormEntity->getMinutes()) {
            /**
             * @var $form Form
             */
            $res = $timeManagementRep->getJobListWithCountAndMinutes($currentFormEntity->getJobKind(), $currentFormEntity->getCountCooks(), $currentFormEntity->getMinutes());
            dump($res);
            /**
             * @var $constraint IsMinutesExistInJobKindWithCookCount
             */
            if (!empty($res)) $this->context->addViolation($constraint->message, [
                '{{ minutes }}'     => $currentFormEntity->getMinutes(),
                '{{ job }}'         => $currentFormEntity->getJobKind()->getJob(),
                '{{ cookCount }}'   => $currentFormEntity->getCountCooks(),
            ]);
        }
    }

    public function getEntityManager()
    {
        return $this->em;
    }

}