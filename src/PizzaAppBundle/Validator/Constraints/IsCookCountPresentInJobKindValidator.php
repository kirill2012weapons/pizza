<?php

namespace PizzaAppBundle\Validator\Constraints;


use Doctrine\ORM\EntityManager;
use PizzaAppBundle\Entity\Cook;
use PizzaAppBundle\Entity\Workload;
use PizzaAppBundle\Entity\WorkloadDefault;
use PizzaAppBundle\Repository\WorkLoadDefaultRepository;
use PizzaAppBundle\Repository\WorkLoadRepository;
use Symfony\Component\Form\Form;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class IsCookCountPresentInJobKindValidator extends ConstraintValidator
{

    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    public function validate($value, Constraint $constraint)
    {
        /**
         * @var $currentFormEntity WorkloadDefault
         */
        $currentFormEntity = $this->context->getObject();
        /**
         * @var $repWorkLoadsDefaultRep WorkLoadDefaultRepository
         */
        $repWorkLoadsDefaultRep = $this->getEntityManager()
            ->getRepository(WorkloadDefault::class);

        $res = [];

        if (is_null($currentFormEntity->getJobKind())) return;

        $res = $repWorkLoadsDefaultRep->getDefaultCookCount(
            $currentFormEntity->getJobKind()->getId(),
            $currentFormEntity->getCooksCount()
        );

            /**
             * @var $constraint IsCookCountExistInCurrentJobKind
             */
            if (!empty($res)) $this->context->addViolation($constraint->message, [
                '{{ value }}'       => $value,
                '{{ kind }}'        => $currentFormEntity->getJobKind()->getJob(),
            ]);
    }

    public function getEntityManager()
    {
        return $this->em;
    }

}