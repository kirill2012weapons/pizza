<?php

namespace PizzaAppBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use PizzaAppBundle\Exception\PizzaException;
use Symfony\Component\Validator\Constraints as Assert;
use PizzaAppBundle\Validator\Constraints as AcmeAssert;

/**
 * Class TimeManagement
 * @package PizzaAppBundle\Entity
 * @ORM\Entity(repositoryClass="PizzaAppBundle\Repository\TimeManagementRepository")
 * @ORM\Table(
 *     name="tbl_time_managment"
 * )
 * @ORM\HasLifecycleCallbacks()
 */
class TimeManagement
{

    /**
     * @var integer
     * @ORM\Id()
     * @ORM\Column(
     *     type="integer"
     * )
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="JobKind", inversedBy="timeManagements")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     * @AcmeAssert\IsMinutesExistInJobKindWithCookCount()
     */
    private $jobKind;

    /**
     * @var integer
     * @ORM\Column(
     *     type="smallint",
     *     name="count_cooks",
     *     length=2,
     *     nullable=false
     * )
     * @Assert\Length(
     *     max="2",
     *     maxMessage="Not mor then 2 symbols",
     *     min="1",
     *     minMessage="Not less then 1 symbol"
     * )
     * @Assert\Regex(
     *     pattern="/^[1-9][0-9]?$/",
     *     message="Integer From 1 To 99"
     * )
     * @Assert\NotNull(
     *     message="Cooks Count must not be blank!"
     * )
     */
    private $countCooks;

    /**
     * @var integer
     * @ORM\Column(
     *     type="integer",
     *     name="minutes",
     *     length=3,
     *     nullable=false
     * )
     * @Assert\Length(
     *     max="3",
     *     maxMessage="Not mor then 3 symbols",
     *     min="1",
     *     minMessage="Not less then 1 symbol"
     * )
     * @Assert\Regex(
     *     pattern="/^[1-9][0-9]?[0-9]?$/",
     *     message="Integer From 1 To 999"
     * )
     * @Assert\NotNull(
     *     message="Minuts must be present."
     * )
     */
    private $minutes;

    /**
     * @var \DateTime
     * @ORM\Column(
     *     type="datetime",
     *     name="created_at",
     *     length=19,
     *     nullable=false
     * )
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(
     *     type="datetime",
     *     name="modified_at",
     *     length=19,
     *     nullable=true
     * )
     */
    private $modifiedAt;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return JobKind
     */
    public function getJobKind()
    {
        return $this->jobKind;
    }

    /**
     * @param JobKind $jobKind
     */
    public function setJobKind($jobKind)
    {
        $this->jobKind = $jobKind;
    }

    /**
     * @return int
     */
    public function getCountCooks()
    {
        return $this->countCooks;
    }

    /**
     * @param int $countCooks
     */
    public function setCountCooks($countCooks)
    {
        $this->countCooks = $countCooks;
    }

    /**
     * @return int
     */
    public function getMinutes()
    {
        return $this->minutes;
    }

    /**
     * @param int $minutes
     */
    public function setMinutes($minutes)
    {
        $this->minutes = $minutes;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * @param \DateTime $modifiedAt
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;
    }

    /**
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        try {
            $this->setModifiedAt(new \DateTime(date('Y-m-d H:i:s')));

            if($this->getCreatedAt() == null)
            {
                $this->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
            }
        } catch (\Exception $exception) {
            throw new PizzaException($exception->getMessage());
        }
    }

}