<?php

namespace PizzaAppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use PizzaAppBundle\Exception\PizzaException;
use Symfony\Component\Validator\Constraints as Assert;
use PizzaAppBundle\Validator\Constraints as AcmeAssert;

/**
 * Class Workload
 * @package PizzaAppBundle\Entity
 * @ORM\Entity(repositoryClass="PizzaAppBundle\Repository\WorkLoadRepository")
 * @ORM\Table(
 *     name="tbl_workloads"
 * )
 * @ORM\HasLifecycleCallbacks()
 */
class Workload
{

    /**
     * @var integer
     * @ORM\Id()
     * @ORM\Column(
     *     type="integer"
     * )
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * @var JobKind
     * @ORM\ManyToOne(
     *     targetEntity="JobKind",
     *     inversedBy="workLoads"
     * )
     * @ORM\JoinColumn(
     *     name="job_kind_id",
     *     referencedColumnName="id"
     * )
     */
    private $jobKind;

    /**
     * @var integer
     * @ORM\Column(
     *     type="smallint",
     *     name="cooks_count",
     *     length=2,
     *     nullable=false
     * )
     * @Assert\NotBlank(
     *     message="Count of Cook must not be blanck"
     * )
     * @Assert\Regex(
     *     pattern="/^[0-9]{1,2}$/",
     *     message="{{ value }} - not correct. Must be number from 1 to maximum of the cooks count"
     * )
     * @Assert\Range(
     *     min="1",
     *     max="99",
     *     minMessage="Is so lower From 1 must!",
     *     maxMessage="Is hire bro."
     * )
     * @AcmeAssert\IsCookCountExistInCurrentJobKind()
     */
    private $cooksCount;

    /**
     * @var \DateTime
     * @ORM\Column(
     *     type="date",
     *     name="date",
     *     length=10,
     *     nullable=false
     * )
     * @Assert\NotBlank(
     *     message="Date must not be blank!"
     * )
     * @Assert\Date(
     *     message="Wrong Date!"
     * )
     */
    private $date;

    /**
     * @var \DateTime
     * @ORM\Column(
     *     type="datetime",
     *     name="created_at",
     *     length=19,
     *     nullable=false
     * )
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(
     *     type="datetime",
     *     name="modified_at",
     *     length=19,
     *     nullable=true
     * )
     */
    private $modifiedAt;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return JobKind
     */
    public function getJobKind()
    {
        return $this->jobKind;
    }

    /**
     * @param JobKind $jobKind
     */
    public function setJobKind($jobKind)
    {
        $this->jobKind = $jobKind;
    }

    /**
     * @return int
     */
    public function getCooksCount()
    {
        return $this->cooksCount;
    }

    /**
     * @param int $cooksCount
     */
    public function setCooksCount($cooksCount)
    {
        $this->cooksCount = $cooksCount;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param string $date
     */
    public function setDate($date)
    {
        $this->date = new \DateTime($date);
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * @param \DateTime $modifiedAt
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;
    }

    /**
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        try {
            $this->setModifiedAt(new \DateTime(date('Y-m-d H:i:s')));

            if($this->getCreatedAt() == null)
            {
                $this->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
            }
        } catch (\Exception $exception) {
            throw new PizzaException($exception->getMessage());
        }
    }

}