<?php

namespace PizzaAppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use PizzaAppBundle\Exception\PizzaException;
use PizzaAppBundle\Validator\Constraints as AcmeAssert;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class WorkloadDefault
 * @package PizzaAppBundle\Entity
 * @ORM\Entity(repositoryClass="PizzaAppBundle\Repository\WorkLoadDefaultRepository")
 * @ORM\Table(
 *     name="tbl_default_workloads"
 * )
 * @ORM\HasLifecycleCallbacks()
 */
class WorkloadDefault
{

    /**
     * @var integer
     * @ORM\Id()
     * @ORM\Column(
     *     type="integer"
     * )
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * @var JobKind
     * @ORM\ManyToOne(
     *     targetEntity="JobKind",
     *     inversedBy="workLoadsDefault"
     * )
     * @ORM\JoinColumn(
     *     name="job_kind_id",
     *     referencedColumnName="id"
     * )
     * @AcmeAssert\IsJobExistsInDefaultWorkLoad(groups={"persists"})
     */
    private $jobKind;

    /**
     * @var integer
     * @ORM\Column(
     *     type="smallint",
     *     name="cooks_count",
     *     length=2,
     *     nullable=false
     * )
     * @AcmeAssert\IsCookCountPresentInJobKind()
     */
    private $cooksCount;

    /**
     * @var \DateTime
     * @ORM\Column(
     *     type="datetime",
     *     name="created_at",
     *     length=19,
     *     nullable=false
     * )
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(
     *     type="datetime",
     *     name="modified_at",
     *     length=19,
     *     nullable=true
     * )
     */
    private $modifiedAt;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return JobKind
     */
    public function getJobKind()
    {
        return $this->jobKind;
    }

    /**
     * @param JobKind $jobKind
     */
    public function setJobKind($jobKind)
    {
        $this->jobKind = $jobKind;
    }

    /**
     * @return int
     */
    public function getCooksCount()
    {
        return $this->cooksCount;
    }

    /**
     * @param int $cooksCount
     */
    public function setCooksCount($cooksCount)
    {
        $this->cooksCount = $cooksCount;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * @param \DateTime $modifiedAt
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;
    }

    /**
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        try {
            $this->setModifiedAt(new \DateTime(date('Y-m-d H:i:s')));

            if($this->getCreatedAt() == null)
            {
                $this->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
            }
        } catch (\Exception $exception) {
            throw new PizzaException($exception->getMessage());
        }
    }

}