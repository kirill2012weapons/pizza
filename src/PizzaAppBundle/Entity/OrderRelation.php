<?php

namespace PizzaAppBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use PizzaAppBundle\Exception\PizzaException;

/**
 * Class OrderRelation
 * @package PizzaAppBundle\Entity
 * @ORM\Entity()
 * @ORM\Table(
 *     name="tbl_order_relations"
 * )
 * @ORM\HasLifecycleCallbacks()
 */
class OrderRelation
{

    /**
     * @var integer
     * @ORM\Id()
     * @ORM\Column(
     *     type="integer"
     * )
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * @var Order
     * @ORM\ManyToOne(
     *     targetEntity="Order",
     *     inversedBy="orderRelations"
     * )
     * @ORM\JoinColumn(
     *     name="order_id",
     *     referencedColumnName="id"
     * )
     */
    private $order;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(
     *     targetEntity="Cook",
     *     mappedBy="orderRelations"
     * )
     */
    private $cooks;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(
     *     targetEntity="Product",
     *     mappedBy="orderRelations"
     * )
     */
    private $products;

    public function __construct()
    {
        $this->products     =       new ArrayCollection();
        $this->cooks        =       new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param Order $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    /**
     * @return ArrayCollection
     */
    public function getCooks()
    {
        return $this->cooks;
    }

    /**
     * @param Cook $cook
     */
    public function setCooks($cook)
    {
        $this->cooks->add($cook);
    }

    /**
     * @return ArrayCollection
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param Product $product
     */
    public function setProducts($product)
    {
        $this->products->add($product);
    }

    /**
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        try {
            $this->setModifiedAt(new \DateTime(date('Y-m-d H:i:s')));

            if($this->getCreatedAt() == null)
            {
                $this->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
            }
        } catch (\Exception $exception) {
            throw new PizzaException($exception->getMessage());
        }
    }

}