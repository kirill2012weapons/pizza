<?php

namespace PizzaAppBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use PizzaAppBundle\Exception\PizzaException;

/**
 * Class Order
 * @package PizzaAppBundle\Entity
 * @ORM\Entity()
 * @ORM\Table(
 *     name="tbl_orders"
 * )
 * @ORM\HasLifecycleCallbacks()
 */
class Order
{

    const START_COOKING =   1;

    const END_COOKING =     2;

    const IN_SENDING =      3;

    const RECEIVE =         4;

    /**
     * @var integer
     * @ORM\Id()
     * @ORM\Column(
     *     type="integer"
     * )
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(
     *     targetEntity="OrderRelation",
     *     mappedBy="order"
     * )
     */
    private $orderRelations;

    /**
     * @var \DateTime
     * @ORM\Column(
     *     type="datetime",
     *     name="time_start",
     *     length=19,
     *     nullable=false
     * )
     */
    private $timeStart;

    /**
     * @var \DateTime
     * @ORM\Column(
     *     type="datetime",
     *     name="time_end",
     *     length=19,
     *     nullable=false
     * )
     */
    private $timeEnd;

    /**
     * @var integer
     * @ORM\Column(
     *     type="smallint",
     *     name="send_order",
     *     length=2,
     *     nullable=false
     * )
     */
    private $sendOrder = self::START_COOKING;

    /**
     * @var \DateTime
     * @ORM\Column(
     *     type="datetime",
     *     name="created_at",
     *     length=19,
     *     nullable=false
     * )
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(
     *     type="datetime",
     *     name="modified_at",
     *     length=19,
     *     nullable=true
     * )
     */
    private $modifiedAt;

    public function __construct()
    {
        $this->orderRelations       =       new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return ArrayCollection
     */
    public function getOrderRelations()
    {
        return $this->orderRelations;
    }

    /**
     * @param OrderRelation $orderRelation
     */
    public function setOrderRelations($orderRelation)
    {
        $this->orderRelations->add($orderRelation);
    }

    /**
     * @return \DateTime
     */
    public function getTimeStart()
    {
        return $this->timeStart;
    }

    /**
     * @param \DateTime $timeStart
     */
    public function setTimeStart($timeStart)
    {
        $this->timeStart = $timeStart;
    }

    /**
     * @return \DateTime
     */
    public function getTimeEnd()
    {
        return $this->timeEnd;
    }

    /**
     * @param \DateTime $timeEnd
     */
    public function setTimeEnd($timeEnd)
    {
        $this->timeEnd = $timeEnd;
    }

    /**
     * @return int
     */
    public function getSendOrder()
    {
        return $this->sendOrder;
    }

    /**
     * @param int $sendOrder
     */
    public function setSendOrder($sendOrder)
    {
        $this->sendOrder = $sendOrder;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * @param \DateTime $modifiedAt
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;
    }

    /**
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        try {
            $this->setModifiedAt(new \DateTime(date('Y-m-d H:i:s')));

            if($this->getCreatedAt() == null)
            {
                $this->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
            }
        } catch (\Exception $exception) {
            throw new PizzaException($exception->getMessage());
        }
    }

}