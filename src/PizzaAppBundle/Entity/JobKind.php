<?php

namespace PizzaAppBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use PizzaAppBundle\Exception\PizzaException;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class JobKind
 * @package PizzaAppBundle\Entity
 * @ORM\Entity(repositoryClass="PizzaAppBundle\Repository\JobKindRepository")
 * @ORM\Table(
 *     name="tbl_job_kinds"
 * )
 * @ORM\HasLifecycleCallbacks()
 */
class JobKind
{

    /**
     * @var integer
     * @ORM\Id()
     * @ORM\Column(
     *     type="integer"
     * )
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(
     *     targetEntity="Cook",
     *     mappedBy="jobKind"
     * )
     */
    private $cooks;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(
     *     targetEntity="Workload",
     *     mappedBy="jobKind"
     * )
     */
    private $workLoads;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(
     *     targetEntity="WorkloadDefault",
     *     mappedBy="jobKind"
     * )
     */
    private $workLoadsDefault;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(
     *     targetEntity="Product",
     *     mappedBy="jobKind"
     * )
     */
    private $products;

    /**
     * Many Users have Many Groups.
     * @ORM\OneToMany(targetEntity="TimeManagement", mappedBy="jobKind")
     */
    private $timeManagements;

    /**
     * @var string
     * @ORM\Column(
     *     type="string",
     *     name="job",
     *     length=100,
     *     nullable=false
     * )
     * @Assert\NotBlank(
     *     message="Job Name Must not be empty."
     * )
     * @Assert\Length(
     *     max="100",
     *     maxMessage="{{ value }} - must not be hire the 100 symbols",
     *     min="2",
     *     minMessage="{{ value }} - must not be lower then 2 symbols"
     * )
     * @Assert\Regex(
     *     pattern="/^[A-Z][a-zA-Z\s\-\_]+[a-z]+$/",
     *     message="{{ value }} - Can include whitespaces, '-', '_', numbers, and latin symbols, First symbol must be in uppercase."
     * )
     */
    private $job;

    /**
     * @var string
     * @ORM\Column(
     *     type="text",
     *     name="job_description",
     *     length=2000,
     *     nullable=true
     * )
     * @Assert\NotBlank(
     *     message="Description Name Must not be empty."
     * )
     * @Assert\Length(
     *     max="2000",
     *     maxMessage="{{ value }} - must not be hire the 2000 symbols",
     *     min="5",
     *     minMessage="{{ value }} - must not be lower then 5 symbols"
     * )
     * @Assert\Regex(
     *     pattern="/^[a-zA-Z][a-zA-Z0-9\s\-\_\.\,]+[a-z\.\!\?]+$/",
     *     message="{{ value }} - Can include whitespaces, '-', '_', numbers, and latin symbols, First symbol must be in uppercase."
     * )
     */
    private $jobDescription;

    /**
     * @var \DateTime
     * @ORM\Column(
     *     type="datetime",
     *     name="created_at",
     *     length=19,
     *     nullable=false
     * )
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(
     *     type="datetime",
     *     name="modified_at",
     *     length=19,
     *     nullable=true
     * )
     */
    private $modifiedAt;

    public function __construct()
    {
        $this->cooks                        =       new ArrayCollection();
        $this->workLoads                    =       new ArrayCollection();
        $this->workLoadsDefault             =       new ArrayCollection();
        $this->products                     =       new ArrayCollection();
        $this->timeManagements              =       new ArrayCollection();
    }

    public function __toString() {
        return $this->getJob();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return ArrayCollection
     */
    public function getCooks()
    {
        return $this->cooks;
    }

    /**
     * @param Cook $cook
     */
    public function setCooks($cook)
    {
        $this->cooks->add($cook);
    }

    /**
     * @return ArrayCollection
     */
    public function getWorkLoads()
    {
        return $this->workLoads;
    }

    /**
     * @param Workload $workLoad
     */
    public function setWorkLoads($workLoad)
    {
        $this->workLoads->add($workLoad);
    }

    /**
     * @return ArrayCollection
     */
    public function getWorkLoadsDefault()
    {
        return $this->workLoadsDefault;
    }

    /**
     * @param WorkloadDefault $workLoadDefault
     */
    public function setWorkLoadsDefault($workLoadDefault)
    {
        $this->workLoadsDefault->add($workLoadDefault);
    }

    /**
     * @return ArrayCollection
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param Product $product
     */
    public function setProducts($product)
    {
        $this->products->add($product);
    }

    /**
     * @return ArrayCollection
     */
    public function getTimeManagements()
    {
        return $this->timeManagements;
    }

    /**
     * @param TimeManagement $timeManagement
     */
    public function setTimeManagements($timeManagement)
    {
        $this->timeManagements->add($timeManagement);
    }

    /**
     * @return string
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * @param string $job
     */
    public function setJob($job)
    {
        $this->job = $job;
    }

    /**
     * @return string
     */
    public function getJobDescription()
    {
        return $this->jobDescription;
    }

    /**
     * @param string $jobDescription
     */
    public function setJobDescription($jobDescription)
    {
        $this->jobDescription = $jobDescription;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * @param \DateTime $modifiedAt
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;
    }

    /**
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        try {
            $this->setModifiedAt(new \DateTime(date('Y-m-d H:i:s')));

            if($this->getCreatedAt() == null)
            {
                $this->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
            }
        } catch (\Exception $exception) {
            throw new PizzaException($exception->getMessage());
        }
    }

}