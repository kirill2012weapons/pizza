<?php

namespace PizzaAppBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use PizzaAppBundle\Exception\PizzaException;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Cook
 * @package PizzaAppBundle\Entity
 * @ORM\Entity(repositoryClass="PizzaAppBundle\Repository\CookRepository")
 * @ORM\Table(
 *     name="tbl_cooks"
 * )
 * @ORM\HasLifecycleCallbacks()
 */
class Cook
{
    /**
     * @var integer
     * @ORM\Id()
     * @ORM\Column(
     *     type="integer"
     * )
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * @var JobKind
     * @ORM\ManyToOne(
     *     targetEntity="JobKind",
     *     inversedBy="cooks"
     * )
     * @ORM\JoinColumn(
     *     name="job_kind_id",
     *     referencedColumnName="id"
     * )
     */
    private $jobKind;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(
     *     targetEntity="OrderRelation",
     *     inversedBy="cooks"
     * )
     * @ORM\JoinTable(
     *     name="cooks_order_relations"
     * )
     */
    private $orderRelations;

    /**
     * @var string
     * @ORM\Column(
     *     type="string",
     *     name="name",
     *     length=100,
     *     nullable=false
     * )
     * @Assert\NotBlank(
     *     message="Name must not be blank."
     * )
     * @Assert\Regex(
     *     pattern="/^[A-Za-z\s\-]+$/",
     *     message="{{ value }} - not correct. Can contains spaces and '-' and latin A-Za-z symbols."
     * )
     * @Assert\Length(
     *     max="100",
     *     maxMessage="{{ value }} - not Correct. Not hire then 100 symbols.",
     *     min="2",
     *     minMessage="{{ value }} - not Correct. Not lower then 2 symbols."
     * )
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(
     *     type="string",
     *     name="surname",
     *     length=100,
     *     nullable=false
     * )
     * @Assert\NotBlank(
     *     message="Surname must not be blank."
     * )
     * @Assert\Regex(
     *     pattern="/^[A-Za-z\s\-]+$/",
     *     message="{{ value }} - not correct. Can contains spaces and '-' and latin A-Za-z symbols."
     * )
     * @Assert\Length(
     *     max="100",
     *     maxMessage="{{ value }} - not Correct. Not hire then 100 symbols.",
     *     min="2",
     *     minMessage="{{ value }} - not Correct. Not lower then 2 symbols."
     * )
     */
    private $surname;

    /**
     * @var integer
     * @ORM\Column(
     *     type="smallint",
     *     name="age",
     *     length=2,
     *     nullable=false
     * )
     * @Assert\NotBlank(
     *     message="Age must not be blank."
     * )
     * @Assert\Regex(
     *     pattern="/^[1-9][0-9]$/",
     *     message="{{ value }} - not correct. Can contains only numbers."
     * )
     * @Assert\Range(
     *     min="16",
     *     minMessage="Age is small. Must ber hire then 16.",
     *     max="99",
     *     maxMessage="Age is not correct. People do not live so long in Ukraine"
     * )
     */
    private $age;

    /**
     * @var string
     * @ORM\Column(
     *     type="text",
     *     name="living_place",
     *     length=2000,
     *     nullable=false
     * )
     * @Assert\NotBlank(
     *     message="Living Place must not be blank."
     * )
     * @Assert\Regex(
     *     pattern="/^[A-Za-z0-9\s\!\?\.\,\/\x22\x27\:\;\#\(\)\-]+$/",
     *     message="{{ value }} - not correct. Can contains spaces and '-' and latin A-Za-z symbols."
     * )
     * @Assert\Length(
     *     max="2000",
     *     maxMessage="{{ value }} - not Correct. Not hire then 2000 symbols.",
     *     min="10",
     *     minMessage="{{ value }} - not Correct. Not lower then 10 symbols."
     * )
     */
    private $livingPlace;

    /**
     * @var \DateTime
     * @ORM\Column(
     *     type="datetime",
     *     name="created_at",
     *     length=19,
     *     nullable=false
     * )
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(
     *     type="datetime",
     *     name="modified_at",
     *     length=19,
     *     nullable=true
     * )
     */
    private $modifiedAt;

    public function __construct()
    {
        $this->orderRelations       =       new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return JobKind
     */
    public function getJobKind()
    {
        return $this->jobKind;
    }

    /**
     * @param JobKind $jobKind
     */
    public function setJobKind($jobKind)
    {
        $this->jobKind = $jobKind;
    }

    /**
     * @return ArrayCollection
     */
    public function getOrderRelations()
    {
        return $this->orderRelations;
    }

    /**
     * @param OrderRelation $orderRelation
     */
    public function setOrderRelations($orderRelation)
    {
        $this->orderRelations->add($orderRelation);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

    /**
     * @return int
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @param int $age
     */
    public function setAge($age)
    {
        $this->age = $age;
    }

    /**
     * @return string
     */
    public function getLivingPlace()
    {
        return $this->livingPlace;
    }

    /**
     * @param string $livingPlace
     */
    public function setLivingPlace($livingPlace)
    {
        $this->livingPlace = $livingPlace;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * @param \DateTime $modifiedAt
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;
    }

    /**
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        try {
            $this->setModifiedAt(new \DateTime(date('Y-m-d H:i:s')));

            if($this->getCreatedAt() == null)
            {
                $this->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
            }
        } catch (\Exception $exception) {
            throw new PizzaException($exception->getMessage());
        }
    }

}