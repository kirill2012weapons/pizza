<?php

namespace PizzaAppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use PizzaAppBundle\Exception\PizzaException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Vich\UploaderBundle\Entity\File as EmbeddedFile;

/**
 * Class ProductAttachment
 * @package PizzaAppBundle\Entity
 * @ORM\Entity()
 * @ORM\Table(
 *     name="tbl_product_attachments",
 *     indexes={
 *          @ORM\Index(name="type_idx", columns={"type"})
 *     }
 * )
 * @ORM\HasLifecycleCallbacks()
 * @Vich\Uploadable()
 */
class ProductAttachment
{
    const TITLE_IMAGE = 'title_image';
    const GALLERY_IMAGE = 'gallery_image';

    /**
     * @var integer
     * @ORM\Id()
     * @ORM\Column(
     *     type="integer"
     * )
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * @var Product
     * @ORM\ManyToOne(
     *     targetEntity="Product",
     *     inversedBy="productAttachments",
     *     cascade={"all"}
     * )
     * @ORM\JoinColumn(
     *     name="product_id",
     *     referencedColumnName="id"
     * )
     */
    private $product;

    /**
     * @var string
     * @ORM\Column(
     *     type="string",
     *     name="type",
     *     nullable=false
     * )
     * @Assert\Choice(
     *     choices={
     *          ProductAttachment::GALLERY_IMAGE,
     *          ProductAttachment::TITLE_IMAGE
     *     },
     *     message="Must me supported type"
     * )
     */
    private $type;

    /**
     * @var File
     * @Vich\UploadableField(
     *     mapping="upload_dir",
     *     fileNameProperty="fileName.name",
     *     size="fileName.size",
     *     mimeType="fileName.mimeType",
     *     originalName="fileName.originalName",
     *     dimensions="fileName.dimensions"
     * )
     */
    private $file;

    /**
     * @ORM\Embedded(class="Vich\UploaderBundle\Entity\File")
     *
     * @var EmbeddedFile
     */
    private $fileName;

    /**
     * @var \DateTime
     * @ORM\Column(
     *     type="datetime",
     *     name="created_at",
     *     length=19,
     *     nullable=false
     * )
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(
     *     type="datetime",
     *     name="modified_at",
     *     length=19,
     *     nullable=true
     * )
     */
    private $modifiedAt;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $updatedAt;

    public function __construct()
    {
        $this->fileName = new EmbeddedFile();
    }

    /**
     * @return File
     */
    public function getFile(): ?File
    {
        return $this->file;
    }

    /**
     * @param File $file
     * @throws
     */
    public function setFile(?File $file = null)
    {
        $this->file = $file;
        if (null !== $file) {
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    /**
     * @return EmbeddedFile
     */
    public function getFileName(): ?EmbeddedFile
    {
        return $this->fileName;
    }

    /**
     * @param EmbeddedFile $fileName
     */
    public function setFileName(EmbeddedFile $fileName)
    {
        $this->fileName = $fileName;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param Product $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * @param \DateTime $modifiedAt
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;
    }

    /**
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        try {
            $this->setModifiedAt(new \DateTime(date('Y-m-d H:i:s')));

            if($this->getCreatedAt() == null)
            {
                $this->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
            }
        } catch (\Exception $exception) {
            throw new PizzaException($exception->getMessage());
        }
    }

}