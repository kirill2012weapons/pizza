<?php

namespace PizzaAppBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use PizzaAppBundle\Exception\PizzaException;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Product
 * @package PizzaAppBundle\Entity
 * @ORM\Entity(repositoryClass="PizzaAppBundle\Repository\ProductRepository")
 * @ORM\Table(
 *     name="tbl_products"
 * )
 * @ORM\HasLifecycleCallbacks()
 */
class Product
{

    /**
     * @var integer
     * @ORM\Id()
     * @ORM\Column(
     *     type="integer"
     * )
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * @var JobKind
     * @ORM\ManyToOne(
     *     targetEntity="JobKind",
     *     inversedBy="products"
     * )
     * @ORM\JoinColumn(
     *     name="job_kind_id",
     *     referencedColumnName="id"
     * )
     */
    private $jobKind;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(
     *     targetEntity="OrderRelation",
     *     inversedBy="products"
     * )
     * @ORM\JoinTable(
     *     name="products_order_relations"
     * )
     */
    private $orderRelations;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(
     *     targetEntity="ProductAttachment",
     *     mappedBy="product",
     *     cascade={"persist"},
     *     orphanRemoval=true
     * )
     */
    public $productAttachments;

    /**
     * @var string
     * @ORM\Column(
     *     type="string",
     *     name="title",
     *     length=200,
     *     nullable=false
     * )
     * @Assert\NotBlank(
     *     message="Title must not be blank!"
     * )
     * @Assert\Length(
     *     min="5",
     *     minMessage="Title must be minimum 5 symbols!",
     *     max="200",
     *     maxMessage="Title must not be more then 200 symbol length!"
     * )
     * @Assert\Regex(
     *     pattern="/^[A-Za-z0-9\-\_\.\,\:\s]+$/",
     *     message="Can contain '-',':','A-Z','a-z','0-9','_','.',',',':'"
     * )
     */
    private $title;

    /**
     * @var string
     * @ORM\Column(
     *     type="text",
     *     name="description",
     *     length=2000,
     *     nullable=false
     * )
     * @Assert\NotBlank(
     *     message="Description must not be blank!"
     * )
     * @Assert\Length(
     *     min="10",
     *     minMessage="Description must be minimum 5 symbols!",
     *     max="2000",
     *     maxMessage="Description must not be more then 200 symbol length!"
     * )
     * @Assert\Regex(
     *     pattern="/[A-Za-z0-9\-\_\.\,\:\s\0022\0027\^\(\)\+\#\@\!\?\%]/",
     *     message="Can contain '-',':','A-Z','a-z','0-9','_','.',',',':',''','^','()','+','#','@','!','?','%'"
     * )
     */
    private $description;

    /**
     * @var \DateTime
     * @ORM\Column(
     *     type="datetime",
     *     name="created_at",
     *     length=19,
     *     nullable=false
     * )
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(
     *     type="datetime",
     *     name="modified_at",
     *     length=19,
     *     nullable=true
     * )
     */
    private $modifiedAt;

    public function __construct()
    {
        $this->orderRelations               =       new ArrayCollection();
        $this->productAttachments           =       new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return JobKind
     */
    public function getJobKind()
    {
        return $this->jobKind;
    }

    /**
     * @param JobKind $jobKind
     */
    public function setJobKind($jobKind)
    {
        $this->jobKind = $jobKind;
    }

    /**
     * @return ArrayCollection
     */
    public function getProductAttachments()
    {
        return $this->productAttachments;
    }

    /**
     * @param ProductAttachment $productAttachments
     */
    public function addProductAttachments(ProductAttachment $productAttachments)
    {
        $productAttachments->setProduct($this);
        $this->productAttachments->add($productAttachments);
    }


    /**
     * @param ProductAttachment $productAttachment
     */
    public function removeProductAttachments(ProductAttachment $productAttachment)
    {
        if (!$this->productAttachments->contains($productAttachment)) {
            return;
        }
        $this->productAttachments->removeElement($productAttachment);
        $productAttachment->setProduct(null);
    }



    /**
     * @return ArrayCollection
     */
    public function getOrderRelations()
    {
        return $this->orderRelations;
    }

    /**
     * @param OrderRelation $orderRelation
     */
    public function setOrderRelations($orderRelation)
    {
        $this->orderRelations->add($orderRelation);
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * @param \DateTime $modifiedAt
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;
    }

    /**
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        try {
            $this->setModifiedAt(new \DateTime(date('Y-m-d H:i:s')));

            if($this->getCreatedAt() == null)
            {
                $this->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
            }
        } catch (\Exception $exception) {
            throw new PizzaException($exception->getMessage());
        }
    }

}