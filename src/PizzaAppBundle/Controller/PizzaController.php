<?php

namespace PizzaAppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PizzaController extends Controller
{
    public function indexAction()
    {
        return $this->render('@pizza/Pizza/index.html.twig');
    }
}
