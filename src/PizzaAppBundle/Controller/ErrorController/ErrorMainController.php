<?php

namespace PizzaAppBundle\Controller\ErrorController;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ErrorMainController extends Controller
{

    public function somethingIsBrokenAction()
    {
        return $this->render('@error_pizza/something-is-broken.html.twig');
    }

}