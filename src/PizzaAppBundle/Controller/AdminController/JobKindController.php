<?php

namespace PizzaAppBundle\Controller\AdminController;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use PizzaAppBundle\Entity\JobKind;
use PizzaAppBundle\Exception\PizzaException;
use PizzaAppBundle\Form\Admin\JobKind\JobKindType;
use PizzaAppBundle\Repository\JobKindRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class JobKindController extends Controller
{

    public function indexAction()
    {
        /**
         * @var $rep JobKindRepository
         */
        $rep = $this->getDoctrine()->getRepository(JobKind::class);

        return $this->render('@admin_pizza/JobKind/index.html.twig', [
            'result' => $rep->getJobKindsOnlyDESCAsArray(),
        ]);
    }

    public function addAction(Request $request)
    {
        $jobKind = new JobKind();
        $form = $this->createForm(JobKindType::class, $jobKind);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /**
             * @var $em EntityManager
             */
            $em = $this->getDoctrine()->getManager();
            $em->persist($jobKind);
            try {
                $em->flush();
            } catch (\Doctrine\ORM\OptimisticLockException $exception){
                throw new PizzaException($exception->getMessage());
            }
            return $this->redirectToRoute('job_kind_index');
        }
        return $this->render('@admin_pizza/JobKind/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function deleteAction($id)
    {
        /**
         * @var $em EntityManager
         */
        $em = $this
            ->getDoctrine()
            ->getManager();

        try {
            $jobKind = $em
                ->find(JobKind::class, $id);
        } catch (\Exception $exception) {
            throw new PizzaException($exception->getMessage());
        }

        $em->remove($jobKind);
        try {
            $em->flush();
        } catch (\Doctrine\ORM\OptimisticLockException $exception) {
            throw new PizzaException($exception->getMessage());
        }

        return $this->redirectToRoute('job_kind_index');

    }

    public function modifyAction($id, Request $request)
    {
        /**
         * @var $em EntityManager
         */
        $em = $this
            ->getDoctrine()
            ->getManager();

        try {
            $jobKind = $em
                ->find(JobKind::class, $id);
        } catch (\Exception $exception) {
            throw new PizzaException($exception->getMessage());
        }

        $form = $this->createForm(JobKindType::class, $jobKind);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em->flush();
            } catch (\Doctrine\ORM\OptimisticLockException $exception) {
                throw new PizzaException($exception->getMessage());
            }
            return $this->redirectToRoute('job_kind_index');
        }
        return $this->render('@admin_pizza/JobKind/modify.html.twig', [
            'form' => $form->createView(),
        ]);

    }

}