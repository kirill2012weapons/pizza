<?php

namespace PizzaAppBundle\Controller\AdminController;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use PizzaAppBundle\Entity\Workload;
use PizzaAppBundle\Entity\WorkloadDefault;
use PizzaAppBundle\Exception\PizzaException;
use PizzaAppBundle\Form\Admin\WorkLoad\WorkLoadDefaultType;
use PizzaAppBundle\Form\Admin\WorkLoad\WorkLoadType;
use PizzaAppBundle\Repository\WorkLoadDefaultRepository;
use PizzaAppBundle\Repository\WorkLoadRepository;
use PizzaAppBundle\Widgets\Calendar\CalendarService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class WorkLoadController extends Controller
{

    public function indexWorkLoadsAction()
    {

        /**
         * @var $em EntityManager
         */
        $em = $this->getDoctrine()->getManager();

        /**
         * @var $calendar CalendarService
         */
        $calendar = $this->get('calendar');
        $calendar->setEntity(WorkLoad::class);

        /**
         * @var $wLRep WorkLoadRepository
         */
        $wLRep = $em->getRepository(Workload::class);

        return $this->render('@admin_pizza/Workloads/index.html.twig', [
            'workLoads' => $wLRep->getWorkLoadsAllDESCDateDESCJobKindNameAsArray(),
            'calendar' => $calendar,
        ]);
    }

    public function addWorkLoadsAction(Request $request)
    {

        $workLoad = new Workload();
        $form = $this->createForm(WorkLoadType::class, $workLoad);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /**
             * @var $em EntityManager
             */
            $em = $this->getDoctrine()
                ->getManager();

            $em->persist($workLoad);

        try {
            $em->flush();
            return $this->redirectToRoute('workloads_index');
        } catch (\Doctrine\ORM\OptimisticLockException $exception) {
            throw new PizzaException($exception->getMessage());
        }
    }

        return $this->render('@admin_pizza/Workloads/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function deleteWorkLoadsAction($id)
    {
        /**
         * @var $em EntityManager
         */
        $em = $this->getDoctrine()
            ->getManager();

        try {
            $workLoad = $em->find(Workload::class, $id);
        } catch (\Exception $exception) {
            throw new PizzaException($exception->getMessage());
        }

        try {
            $em->remove($workLoad);
            $em->flush();
            return $this->redirectToRoute('workloads_index');
        } catch (\Doctrine\ORM\OptimisticLockException $exception) {
            throw new PizzaException($exception->getMessage());
        }

    }

    public function modifyWorkLoadsAction($id, Request $request)
    {
        /**
         * @var $em EntityManager
         */
        $em = $this->getDoctrine()
            ->getManager();

        try {
            $workLoad = $em->find(Workload::class, $id);
        } catch (\Exception $exception) {
            throw new PizzaException($exception->getMessage());
        }

        $form = $this->createForm(WorkLoadType::class, $workLoad);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em->flush();
                return $this->redirectToRoute('workloads_index');
            } catch (\Doctrine\ORM\OptimisticLockException $exception) {
                throw new PizzaException($exception->getMessage());
            }
        }

        return $this->render('@admin_pizza/Workloads/modify.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function indexWorkLoadsDefaultAction()
    {
        /**
         * @var $em EntityManager
         */
        $em = $this->getDoctrine()->getManager();

        /**
         * @var $wLDefaultRep WorkLoadDefaultRepository
         */
        $wLDefaultRep = $em->getRepository(WorkloadDefault::class);

        return $this->render('@admin_pizza/Workloads/Default/index.html.twig', [
            'workLoadsDefaults' => $wLDefaultRep->getWorkLoadDefaultAsArray(),
        ]);
    }

    public function addWorkLoadsDefaultAction(Request $request)
    {
        $workLoadDefault = new WorkloadDefault();
        $form = $this->createForm(WorkLoadDefaultType::class, $workLoadDefault, [
            'validation_groups' => ['Default', 'persists'],
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /**
             * @var $em EntityManager
             */
            $em = $this->getDoctrine()
                ->getManager();

            $em->persist($workLoadDefault);

            try {
                $em->flush();
                return $this->redirectToRoute('workloads_default_index');
            } catch (\Doctrine\ORM\OptimisticLockException $exception) {
                throw new PizzaException($exception->getMessage());
            }
        }

        return $this->render('@admin_pizza/Workloads/Default/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function deleteWorkLoadsDefaultAction($id, Request $request)
    {
        /**
         * @var $em EntityManager
         */
        $em = $this->getDoctrine()
            ->getManager();

        try {
            $workLoadDefault = $em->find(WorkloadDefault::class, $id);
            $em->remove($workLoadDefault);
            $em->flush();
            return $this->redirectToRoute('workloads_default_index');
        } catch (\Exception $exception) {
            throw new PizzaException($exception->getMessage());
        }

    }

    public function modifyWorkLoadsDefaultAction($id, Request $request)
    {
        /**
         * @var $em EntityManager
         */
        $em = $this->getDoctrine()
            ->getManager();

        try {
            $workLoadDefault = $em->find(WorkloadDefault::class, $id);
        } catch (\Exception $exception) {
            throw new PizzaException($exception->getMessage());
        }

        $form = $this->createForm(WorkLoadDefaultType::class, $workLoadDefault, [
            'validation_groups' => ['Default', 'persists'],
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            try {
                $em->flush();
                return $this->redirectToRoute('workloads_default_index');
            } catch (\Doctrine\ORM\OptimisticLockException $exception) {
                throw new PizzaException($exception->getMessage());
            }
        }
        /**
         * TODO Kirill END THIS!
         */
        return $this->render('@admin_pizza/Workloads/Default/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}