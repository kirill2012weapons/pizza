<?php

namespace PizzaAppBundle\Controller\AdminController;


use Doctrine\ORM\EntityManager;
use PizzaAppBundle\Entity\Cook;
use PizzaAppBundle\Entity\Product;
use PizzaAppBundle\Entity\ProductAttachment;
use PizzaAppBundle\Exception\PizzaException;
use PizzaAppBundle\Form\Admin\Cook\CookJobType;
use PizzaAppBundle\Form\Admin\Cook\CookType;
use PizzaAppBundle\Form\Admin\Product\ProductType;
use PizzaAppBundle\Repository\CookRepository;
use PizzaAppBundle\Repository\ProductRepository;
use PizzaAppBundle\Widgets\Image\FileWorker;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

class ProductController extends Controller
{

    public function indexAction()
    {

    }

    public function addAction(Request $request)
    {
        $product = new Product();
        $attachment = new ProductAttachment();
        $product->addProductAttachments($attachment);

        $form = $this->createForm(ProductType::class, $product);

        $form->handleRequest($request);

        dump($product);

        /**
         * @var $imageWorker FileWorker
         */
        $imageWorker = $this->get('image_worker');
        foreach ($product->getProductAttachments() as $attachment) {
            /**
             * @var $attachment ProductAttachment
             */
            $attachment->setProduct($product);
        }
        if ($form->isSubmitted() && $form->isValid()) {

            /**
             * @var $em EntityManager
             */
            $em = $this->getDoctrine()->getManager();

            $em->persist($product);
            try {
                $em->flush();
            } catch (\Doctrine\ORM\OptimisticLockException $exception) {
                throw new PizzaException($exception->getMessage());
            }

        }
        return $this->render('@admin_pizza/Products/add.html.twig', [
            'form'              => $form->createView(),
            'image_worker'      => $this->get('image_worker'),
            'product'           => $product,
        ]);
    }

    public function deleteAction($id)
    {

    }

    public function modifyAction($id, Request $request)
    {
        /**
         * @var $em EntityManager
         */
        $em = $this->getDoctrine()->getManager();

        /**
         * @var $productRep ProductRepository
         */
        $productRep = $em->getRepository(Product::class);

        $product = $productRep->find($id);

        $form = $this->createForm(ProductType::class, $product);

        $form->handleRequest($request);

        /**
         * @var $imageWorker FileWorker
         */
        $imageWorker = $this->get('image_worker');

        if ($form->isSubmitted() && $form->isValid()) {

//            foreach ($product->getProductAttachments() as $attachment) {
//                /**
//                 * @var $attachment ProductAttachment
//                 */
//                $attachment->setProduct($product);
//            }
            try {
                $em->flush();
            } catch (\Doctrine\ORM\OptimisticLockException $exception) {
                throw new PizzaException($exception->getMessage());
            }

        }
        return $this->render('@admin_pizza/Products/modify.html.twig', [
            'form'              => $form->createView(),
            'product'           => $product,
            'image_worker'      => $this->get('image_worker'),
        ]);
    }

}