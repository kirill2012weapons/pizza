<?php

namespace PizzaAppBundle\Controller\AdminController;


use Doctrine\ORM\EntityManager;
use PizzaAppBundle\Entity\TestImage;
use PizzaAppBundle\Form\Test\TestType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class TestController extends Controller
{
    public function indexAction( Request $request)
    {

        /**
         * @var $em EntityManager
         */
        $em = $this->getDoctrine()->getManager();

        $testImg = $em->find(TestImage::class, 1);
        $form = $this->createForm(TestType::class, $testImg);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($testImg);
            $em->flush();
        }
        dump($testImg);

        return $this->render('@admin_pizza/Test/test.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}