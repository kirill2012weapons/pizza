<?php

namespace PizzaAppBundle\Controller\AdminController;


use Doctrine\ORM\EntityManager;
use PizzaAppBundle\Entity\TimeManagement;
use PizzaAppBundle\Exception\PizzaException;
use PizzaAppBundle\Form\Admin\TimeManagement\TimeManagementType;
use PizzaAppBundle\Repository\TimeManagementRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class TimeManagementController extends Controller
{

    public function indexAction()
    {
        /**
         * @var $timeManagementRep TimeManagementRepository
         */
        $timeManagementRep = $this->getDoctrine()->getManager()->getRepository(TimeManagement::class);

        return $this->render('@admin_pizza/TimeManagement/index.html.twig', [
            'timeManagements' => $timeManagementRep->getListTimeManagementDescDateAsArray(),
        ]);
    }

    public function addAction(Request $request)
    {
        $timeManagement = new TimeManagement();

        $form = $this->createForm(TimeManagementType::class, $timeManagement);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /**
             * @var $em EntityManager
             */
            $em = $this->getDoctrine()->getManager();
            $em->persist($timeManagement);
            try{
                $em->flush();
                return $this->redirectToRoute('time_management_index');
            } catch (\Doctrine\ORM\OptimisticLockException $exception) {
                throw new PizzaException($exception->getMessage());
            }

        }

        return $this->render('@admin_pizza/TimeManagement/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function deleteAction($id)
    {

        /**
         * @var $em EntityManager
         */
        $em = $this->getDoctrine()->getManager();
        /**
         * @var $timeManagementRep TimeManagementRepository
         */
        $timeManagementRep = $this->getDoctrine()->getManager()->getRepository(TimeManagement::class);

        $timeManagement = $timeManagementRep->find($id);

        if ($timeManagement) {

            try{
                $em->remove($timeManagement);
                $em->flush();
            } catch (\Doctrine\ORM\OptimisticLockException $exception) {
                throw new PizzaException($exception->getMessage());
            }

        }
        return $this->redirectToRoute('time_management_index');
    }

    public function modifyAction($id, Request $request)
    {
        /**
         * @var $em EntityManager
         */
        $em = $this->getDoctrine()->getManager();
        /**
         * @var $timeManagementRep TimeManagementRepository
         */
        $timeManagementRep = $this->getDoctrine()->getManager()->getRepository(TimeManagement::class);

        $timeManagement = $timeManagementRep->find($id);
        if (is_null($timeManagement)) throw new PizzaException('No Finder ID for Time Management');

        $form = $this->createForm(TimeManagementType::class, $timeManagement);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            try{
                $em->flush();
                return $this->redirectToRoute('time_management_index');
            } catch (\Doctrine\ORM\OptimisticLockException $exception) {
                throw new PizzaException($exception->getMessage());
            }

        }

        return $this->render('@admin_pizza/TimeManagement/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }

}