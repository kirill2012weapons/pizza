<?php

namespace PizzaAppBundle\Controller\AdminController;


use Doctrine\ORM\EntityManager;
use PizzaAppBundle\Entity\Cook;
use PizzaAppBundle\Exception\PizzaException;
use PizzaAppBundle\Form\Admin\Cook\CookJobType;
use PizzaAppBundle\Form\Admin\Cook\CookType;
use PizzaAppBundle\Repository\CookRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class CookController extends Controller
{

    public function indexAction()
    {
        /**
         * @var $em EntityManager
         */
        $em = $this->getDoctrine()
            ->getManager();

        /**
         * @var $cooksRep CookRepository
         */
        $cooksRep = $em->getRepository(Cook::class);

        return $this->render('@admin_pizza/Cook/index.html.twig', [
            'cooks' => $cooksRep->getCooksDESCModifiedAsArray(),
        ]);
    }

    public function addAction(Request $request)
    {

        $cook = new Cook();

        $form = $this->createForm(CookType::class, $cook);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /**
             * @var $em EntityManager
             */
            $em = $this->getDoctrine()
                ->getManager();
            $em->persist($cook);

            try {
                $em->flush();
            } catch (\Doctrine\ORM\OptimisticLockException $exception) {
                throw new PizzaException();
            }

            return $this->redirectToRoute('cook_index');
        }

        return $this->render('@admin_pizza/Cook/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function deleteAction($id)
    {
        /**
         * @var $em EntityManager
         */
        $em = $this->getDoctrine()
            ->getManager();

        try {
            $cook = $em->find(Cook::class, $id);
            $em->remove($cook);
        } catch (\Exception $exception) {
            throw new PizzaException($exception->getMessage());
        }

        try {
            $em->flush();
        } catch (\Doctrine\ORM\OptimisticLockException $exception) {
            throw new PizzaException($exception->getMessage());
        }

        return $this->redirectToRoute('cook_index');
    }

    public function modifyAction($id, Request $request)
    {

        /**
         * @var $em EntityManager
         */
        $em = $this->getDoctrine()
            ->getManager();

        try {
            $cook = $em->find(Cook::class, $id);
        } catch (\Exception $exception) {
            throw new PizzaException($exception->getMessage());
        }

        $form = $this->createForm(CookType::class, $cook);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em->flush();
            } catch (\Doctrine\ORM\OptimisticLockException $exception) {
                throw new PizzaException();
            }
            return $this->redirectToRoute('cook_index');
        }

        return $this->render('@admin_pizza/Cook/modify.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function jobsAction(Request $request)
    {

        /**
         * @var $em EntityManager
         */
        $em = $this->getDoctrine()
            ->getManager();

        /**
         * @var $rep CookRepository
         */
        $rep = $em->getRepository(Cook::class);

        $_massCool = $rep->getCooksDESCModifiedAsObj();
        $_massForm = [];

        foreach ($_massCool as $cook) {
            $form = $this->createForm(CookJobType::class, $cook);
            if ($request->get('cook_job_type') && ($request->get('cook_job_type')['id'] == $form->getNormData()->getId())) {
                $form->handleRequest($request);
                if ($form->isSubmitted() && $form->isValid()) {
                    $em->persist($cook);
                    try {
                        $em->flush();
                    } catch (\Doctrine\ORM\OptimisticLockException $exception) {
                        throw new PizzaException($exception->getMessage());
                    }
                }
            }
            $_massForm[] = $form->createView();
        }

        return $this->render('@admin_pizza/Cook/jobs.html.twig', [
            '_massForm'     => $_massForm,
        ]);
    }

}