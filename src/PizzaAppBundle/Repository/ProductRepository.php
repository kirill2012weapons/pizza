<?php

namespace PizzaAppBundle\Repository;


use Doctrine\ORM\EntityRepository;
use PizzaAppBundle\Entity\Product;
use PizzaAppBundle\Exception\PizzaException;

class ProductRepository extends EntityRepository
{

    public function getAllProductsDESCForFormAsObj()
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('product')
            ->from(Product::class, 'product')
            ->orderBy('product.modifiedAt', 'DESC');
    }

    public function getProductByIdWithJobKindAndAttachmentsForModifyObj($id)
    {
        try {
            return $this->getEntityManager()
                ->createQueryBuilder()
                ->select('product, productAttachments')
                ->from(Product::class, 'product')
                ->where('product.id = :id')
                ->join('product.productAttachments', 'productAttachments')
                ->orderBy('product.modifiedAt', 'DESC')
                ->setParameters([
                    'id' => $id,
                ])
                ->getQuery()
                ->getSingleResult();
        } catch (\Exception $exception) {
            throw new PizzaException($exception->getMessage());
        }
    }

}