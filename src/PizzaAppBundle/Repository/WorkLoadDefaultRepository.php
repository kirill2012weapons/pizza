<?php

namespace PizzaAppBundle\Repository;


use Doctrine\ORM\EntityRepository;
use PizzaAppBundle\Entity\WorkloadDefault;

class WorkLoadDefaultRepository extends EntityRepository
{

    public function getWorkLoadDefaultAsArray()
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select("work_load, job_kind")
            ->from(WorkloadDefault::class, 'work_load')
            ->join('work_load.jobKind', 'job_kind')
            ->getQuery()
            ->getArrayResult();
    }

    public function getDefaultCookCount($jobId, $count)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select("work_load_default.id")
            ->from(WorkloadDefault::class, 'work_load_default')
            ->where('work_load_default.cooksCount = :countCooks')
            ->join('work_load_default.jobKind', 'job_kind')
            ->andWhere('job_kind.id = :jobId')
            ->setParameters([
                'jobId' => $jobId,
                'countCooks'=> $count,
            ])
            ->getQuery()
            ->getArrayResult();
    }

    public function getDefaultJobIdIfExists($jobId)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select("work_load_default.id")
            ->from(WorkloadDefault::class, 'work_load_default')
            ->join('work_load_default.jobKind', 'job_kind')
            ->andWhere('job_kind.id = :jobId')
            ->setParameters([
                'jobId' => $jobId,
            ])
            ->getQuery()
            ->getArrayResult();
    }

}