<?php

namespace PizzaAppBundle\Repository;


use Doctrine\ORM\EntityRepository;
use PizzaAppBundle\Entity\JobKind;
use PizzaAppBundle\Entity\TimeManagement;

class TimeManagementRepository extends EntityRepository
{

    public function getJobListWithCountAndMinutes($jobID, $count, $minutes)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('job_kind, timeManagements')
            ->from(JobKind::class, 'job_kind')
            ->where('job_kind.id = :jobID')
            ->join('job_kind.timeManagements', 'timeManagements')
            ->andWhere('timeManagements.countCooks = :count AND timeManagements.minutes = :minutes')
            ->setParameters([
                'jobID'     => $jobID,
                'count'     => $count,
                'minutes'   => $minutes,
            ])
            ->getQuery()
            ->getArrayResult();
    }

    public function getListTimeManagementDescDateAsArray()
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('job_kind, timeManagements')
            ->from(JobKind::class, 'job_kind')
            ->join('job_kind.timeManagements', 'timeManagements')
            ->orderBy('timeManagements.countCooks', 'DESC')
            ->getQuery()
            ->getArrayResult();
    }
}