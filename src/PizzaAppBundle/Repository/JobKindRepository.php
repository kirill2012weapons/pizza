<?php

namespace PizzaAppBundle\Repository;


use Doctrine\ORM\EntityRepository;
use PizzaAppBundle\Entity\JobKind;

class JobKindRepository extends EntityRepository
{

    public function getJobKindsOnlyDESCAsArray()
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('job_kind')
            ->from(JobKind::class, 'job_kind')
            ->orderBy('job_kind.modifiedAt', 'DESC')
            ->getQuery()
            ->getArrayResult();
    }

    public function getAllJobsDESCForFormAsObj()
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('job')
            ->from(JobKind::class, 'job')
            ->orderBy('job.modifiedAt', 'DESC');
    }

}