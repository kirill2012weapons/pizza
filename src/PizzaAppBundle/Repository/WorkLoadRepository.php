<?php

namespace PizzaAppBundle\Repository;


use Doctrine\ORM\EntityRepository;
use PizzaAppBundle\Entity\JobKind;
use PizzaAppBundle\Entity\Workload;
use PizzaAppBundle\Widgets\Calendar\Interfaces\ICalendarWorkLoads;

class WorkLoadRepository extends EntityRepository implements ICalendarWorkLoads
{

    public function getCookCountInCurrentJobKindAsArray($job, $countCook, $date)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('job_kind')
            ->from(JobKind::class, 'job_kind')
            ->where('job_kind.id =' . $job)
            ->join('job_kind.workLoads', 'work_loads')
            ->where('work_loads.cooksCount =' . $countCook)
            ->andWhere('work_loads.date =' . '\'' . $date . '\'')
            ->getQuery()
            ->getArrayResult();
    }

    public function getWorkLoadsAllDESCDateDESCJobKindNameAsArray()
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('work_load, job_kind')
            ->from(Workload::class, 'work_load')
            ->join('work_load.jobKind', 'job_kind')
            ->orderBy('work_load.date', 'DESC')
            ->addOrderBy('job_kind.job', 'DESC')
            ->getQuery()
            ->getArrayResult();
    }

    public function getWorkLoadAllAsArray()
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select("work_load.cooksCount AS cooksCount, job_kind.job AS jobName, DATE_FORMAT(work_load.date, '%Y-%m-%d' AS dateLoad")
            ->from(Workload::class, 'work_load')
            ->join('work_load.jobKind', 'job_kind')
            ->orderBy('work_load.date', 'DESC')
            ->addOrderBy('job_kind.job', 'DESC')
            ->getQuery()
            ->getArrayResult();
    }

}