<?php

namespace PizzaAppBundle\Repository;


use Doctrine\ORM\EntityRepository;
use PizzaAppBundle\Entity\Cook;

class CookRepository extends EntityRepository
{

    public function getCooksDESCModifiedAsArray()
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('cooks')
            ->from(Cook::class, 'cooks')
            ->orderBy('cooks.modifiedAt', 'DESC')
            ->getQuery()
            ->getArrayResult();
    }

    public function getCooksDESCModifiedAsObj()
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('cooks')
            ->from(Cook::class, 'cooks')
            ->orderBy('cooks.modifiedAt', 'DESC')
            ->getQuery()
            ->getResult();
    }

}