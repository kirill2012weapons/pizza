<?php

namespace PizzaAppBundle\Widgets\Image;


use PizzaAppBundle\Exception\PizzaException;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileWorker extends AbsFileWorking {

    private $uploadDirImage;

    private $dirName;

    public function setUploadDir($uploadDirImage)
    {
        $this->uploadDirImage = $this->getTargetDirectory() . $uploadDirImage;
        $this->dirName = $uploadDirImage;
    }

    public function getDirName()
    {
        return $this->dirName;
    }

    private function getUploadDir()
    {
        return $this->uploadDirImage;
    }

    public function save(UploadedFile $file)
    {
        $fileName = $this->generateUniqueFileName() . '.' . $file->guessExtension();

        try {
            $file->move(
                $this->getUploadDir(),
                $fileName
            );
            return $fileName;
        } catch (FileException $e) {
            throw new PizzaException($e->getMessage());
        }
    }

    public function getFileObjectByName($name)
    {
        return new File($this->getUploadDir() . '/' . $name);
    }

    public function getUrlGlobal($name)
    {
        return '/uploads' . $this->getDirName() . '/' . $name;
    }

}