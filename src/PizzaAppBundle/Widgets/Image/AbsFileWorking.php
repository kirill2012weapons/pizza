<?php

namespace PizzaAppBundle\Widgets\Image;


use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Routing\RouterInterface;

abstract class AbsFileWorking
{
    private $targetDirectory;

    /**
     * @var RouterInterface
     */
    private $router;

    public function __construct($targetDirectory, $router)
    {
        $this->targetDirectory =    $targetDirectory;
        $this->router =             $router;
    }

    protected function getTargetDirectory()
    {
        return $this->targetDirectory;
    }

    protected function getRouter()
    {
        return $this->router;
    }

    /**
     * @param $file UploadedFile
     * @return bool | string
     */
    static public function getUploadFileUrlBase64($file)
    {
        if (!$file instanceof UploadedFile) return false;
        $contents = file_get_contents($file->getRealPath());
        return 'data:image/png;base64,' . base64_encode($contents);
    }
    static public function getUploadFileUrl($file)
    {
        if (!$file instanceof UploadedFile) return false;
        return $file->getPath() . DIRECTORY_SEPARATOR . $file->getFilename();
    }
    static function getUploadFileBase64($file)
    {
        if (!$file instanceof UploadedFile) return false;
        $contents = file_get_contents($file->getRealPath());
        return 'data:image/png;base64,' . base64_encode($contents);
    }

    protected function generateUniqueFileName()
    {
        return md5(uniqid());
    }

}