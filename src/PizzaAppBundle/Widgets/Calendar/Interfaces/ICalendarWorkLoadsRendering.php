<?php

namespace PizzaAppBundle\Widgets\Calendar\Interfaces;


interface ICalendarWorkLoadsRendering
{
    public function render();
}