<?php

namespace PizzaAppBundle\Widgets\Calendar\Interfaces;


interface ICalendarWorkLoads
{
    public function getWorkLoadAllAsArray();
}