<?php

namespace PizzaAppBundle\Widgets\Calendar;


use PizzaAppBundle\Exception\PizzaException;
use PizzaAppBundle\Widgets\Calendar\Interfaces\ICalendarWorkLoads;
use PizzaAppBundle\Widgets\Calendar\Interfaces\ICalendarWorkLoadsRendering;

class CalendarService extends AbsCalendar implements ICalendarWorkLoadsRendering
{

    public function render()
    {
        try {
            echo $this->getTemplating()->render(
                '@widgets_pizza/Calendar/calendar.html.twig', [
                    'data' => json_encode($this->getDataFromEntity()),
                ]
            );
        } catch (\Twig\Error\Error $exception) {
            throw new PizzaException($exception->getMessage());
        }
    }

}