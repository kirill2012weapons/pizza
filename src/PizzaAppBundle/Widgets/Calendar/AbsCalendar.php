<?php

namespace PizzaAppBundle\Widgets\Calendar;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Entity;
use PizzaAppBundle\Exception\PizzaException;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\PropertyInfo\PropertyInfoExtractor;

abstract class AbsCalendar
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var EntityManager | null
     */
    private $entityClass = null;

    /**
     * @var PropertyInfoExtractor
     */
    private $propertyExtractor;

    /**
     * @var TwigEngine
     */
    private $templating;

    public function __construct($entityManager,$propertyExtractor, $templating)
    {
        $this->entityManager = $entityManager;
        $this->propertyExtractor = $propertyExtractor;
        $this->templating = $templating;
    }

    public function setEntity($entityClass)
    {
        /**
         * @var $_arr []
         */
        $_arr = $this->getExtractor()->getProperties($this->getEntityManager()->getClassMetadata($entityClass)->customRepositoryClassName);
        if (!in_array('workLoadAllAsArray', $_arr)) throw new PizzaException();
        $this->entityClass = $entityClass;

    }

    protected function getDataFromEntity(){
        return $this->getEntityManager()
            ->getRepository($this->entityClass)
            ->getWorkLoadAllAsArray();
    }

    protected function getExtractor()
    {
        return $this->propertyExtractor;
    }

    protected function getEntityManager()
    {
        return $this->entityManager;
    }

    protected function getTemplating()
    {
        return $this->templating;
    }
}