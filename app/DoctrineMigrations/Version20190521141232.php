<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190521141232 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tbl_product_attachments ADD updated_at DATETIME NOT NULL, ADD file_name_name VARCHAR(255) DEFAULT NULL, ADD file_name_original_name VARCHAR(255) DEFAULT NULL, ADD file_name_mime_type VARCHAR(255) DEFAULT NULL, ADD file_name_size INT DEFAULT NULL, ADD file_name_dimensions LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:simple_array)\', DROP file_name');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tbl_product_attachments ADD file_name VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, DROP updated_at, DROP file_name_name, DROP file_name_original_name, DROP file_name_mime_type, DROP file_name_size, DROP file_name_dimensions');
    }
}
