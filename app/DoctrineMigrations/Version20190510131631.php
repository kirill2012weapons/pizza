<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190510131631 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tbl_product_attachments DROP FOREIGN KEY FK_D440DDE7AA8F9B32');
        $this->addSql('DROP INDEX IDX_D440DDE7AA8F9B32 ON tbl_product_attachments');
        $this->addSql('ALTER TABLE tbl_product_attachments CHANGE job_kind_id product_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE tbl_product_attachments ADD CONSTRAINT FK_D440DDE74584665A FOREIGN KEY (product_id) REFERENCES tbl_products (id)');
        $this->addSql('CREATE INDEX IDX_D440DDE74584665A ON tbl_product_attachments (product_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tbl_product_attachments DROP FOREIGN KEY FK_D440DDE74584665A');
        $this->addSql('DROP INDEX IDX_D440DDE74584665A ON tbl_product_attachments');
        $this->addSql('ALTER TABLE tbl_product_attachments CHANGE product_id job_kind_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE tbl_product_attachments ADD CONSTRAINT FK_D440DDE7AA8F9B32 FOREIGN KEY (job_kind_id) REFERENCES tbl_products (id)');
        $this->addSql('CREATE INDEX IDX_D440DDE7AA8F9B32 ON tbl_product_attachments (job_kind_id)');
    }
}
