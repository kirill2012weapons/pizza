<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190422121744 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE tbl_cooks (id INT AUTO_INCREMENT NOT NULL, job_kind_id INT DEFAULT NULL, name VARCHAR(100) NOT NULL, surname VARCHAR(100) NOT NULL, age SMALLINT NOT NULL, living_place TEXT NOT NULL, created_at DATETIME NOT NULL, modified_at DATETIME DEFAULT NULL, INDEX IDX_D91F493DAA8F9B32 (job_kind_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cooks_order_relations (cook_id INT NOT NULL, order_relation_id INT NOT NULL, INDEX IDX_3602E57FB0D5B835 (cook_id), INDEX IDX_3602E57F29E4EEDD (order_relation_id), PRIMARY KEY(cook_id, order_relation_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tbl_job_kinds (id INT AUTO_INCREMENT NOT NULL, job VARCHAR(100) NOT NULL, job_description TEXT DEFAULT NULL, created_at DATETIME NOT NULL, modified_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tbl_orders (id INT AUTO_INCREMENT NOT NULL, time_start DATETIME NOT NULL, time_end DATETIME NOT NULL, send_order SMALLINT NOT NULL, created_at DATETIME NOT NULL, modified_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tbl_order_relations (id INT AUTO_INCREMENT NOT NULL, order_id INT DEFAULT NULL, INDEX IDX_76C7910E8D9F6D38 (order_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tbl_products (id INT AUTO_INCREMENT NOT NULL, job_kind_id INT DEFAULT NULL, title VARCHAR(200) NOT NULL, description TEXT NOT NULL, created_at DATETIME NOT NULL, modified_at DATETIME DEFAULT NULL, INDEX IDX_E489BC77AA8F9B32 (job_kind_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE products_order_relations (product_id INT NOT NULL, order_relation_id INT NOT NULL, INDEX IDX_E0071D3B4584665A (product_id), INDEX IDX_E0071D3B29E4EEDD (order_relation_id), PRIMARY KEY(product_id, order_relation_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tbl_time_managment (id INT AUTO_INCREMENT NOT NULL, count_cooks SMALLINT NOT NULL, minutes INT NOT NULL, created_at DATETIME NOT NULL, modified_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tbl_workloads (id INT AUTO_INCREMENT NOT NULL, job_kind_id INT DEFAULT NULL, cooks_count SMALLINT NOT NULL, date DATE NOT NULL, created_at DATETIME NOT NULL, modified_at DATETIME DEFAULT NULL, INDEX IDX_99430B86AA8F9B32 (job_kind_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tbl_default_workloads (id INT AUTO_INCREMENT NOT NULL, job_kind_id INT DEFAULT NULL, cooks_count SMALLINT NOT NULL, created_at DATETIME NOT NULL, modified_at DATETIME DEFAULT NULL, INDEX IDX_F3A88220AA8F9B32 (job_kind_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tbl_cooks ADD CONSTRAINT FK_D91F493DAA8F9B32 FOREIGN KEY (job_kind_id) REFERENCES tbl_job_kinds (id)');
        $this->addSql('ALTER TABLE cooks_order_relations ADD CONSTRAINT FK_3602E57FB0D5B835 FOREIGN KEY (cook_id) REFERENCES tbl_cooks (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE cooks_order_relations ADD CONSTRAINT FK_3602E57F29E4EEDD FOREIGN KEY (order_relation_id) REFERENCES tbl_order_relations (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tbl_order_relations ADD CONSTRAINT FK_76C7910E8D9F6D38 FOREIGN KEY (order_id) REFERENCES tbl_orders (id)');
        $this->addSql('ALTER TABLE tbl_products ADD CONSTRAINT FK_E489BC77AA8F9B32 FOREIGN KEY (job_kind_id) REFERENCES tbl_job_kinds (id)');
        $this->addSql('ALTER TABLE products_order_relations ADD CONSTRAINT FK_E0071D3B4584665A FOREIGN KEY (product_id) REFERENCES tbl_products (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE products_order_relations ADD CONSTRAINT FK_E0071D3B29E4EEDD FOREIGN KEY (order_relation_id) REFERENCES tbl_order_relations (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tbl_workloads ADD CONSTRAINT FK_99430B86AA8F9B32 FOREIGN KEY (job_kind_id) REFERENCES tbl_job_kinds (id)');
        $this->addSql('ALTER TABLE tbl_default_workloads ADD CONSTRAINT FK_F3A88220AA8F9B32 FOREIGN KEY (job_kind_id) REFERENCES tbl_job_kinds (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE cooks_order_relations DROP FOREIGN KEY FK_3602E57FB0D5B835');
        $this->addSql('ALTER TABLE tbl_cooks DROP FOREIGN KEY FK_D91F493DAA8F9B32');
        $this->addSql('ALTER TABLE tbl_products DROP FOREIGN KEY FK_E489BC77AA8F9B32');
        $this->addSql('ALTER TABLE tbl_workloads DROP FOREIGN KEY FK_99430B86AA8F9B32');
        $this->addSql('ALTER TABLE tbl_default_workloads DROP FOREIGN KEY FK_F3A88220AA8F9B32');
        $this->addSql('ALTER TABLE tbl_order_relations DROP FOREIGN KEY FK_76C7910E8D9F6D38');
        $this->addSql('ALTER TABLE cooks_order_relations DROP FOREIGN KEY FK_3602E57F29E4EEDD');
        $this->addSql('ALTER TABLE products_order_relations DROP FOREIGN KEY FK_E0071D3B29E4EEDD');
        $this->addSql('ALTER TABLE products_order_relations DROP FOREIGN KEY FK_E0071D3B4584665A');
        $this->addSql('DROP TABLE tbl_cooks');
        $this->addSql('DROP TABLE cooks_order_relations');
        $this->addSql('DROP TABLE tbl_job_kinds');
        $this->addSql('DROP TABLE tbl_orders');
        $this->addSql('DROP TABLE tbl_order_relations');
        $this->addSql('DROP TABLE tbl_products');
        $this->addSql('DROP TABLE products_order_relations');
        $this->addSql('DROP TABLE tbl_time_managment');
        $this->addSql('DROP TABLE tbl_workloads');
        $this->addSql('DROP TABLE tbl_default_workloads');
    }
}
