<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190510131552 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tbl_product_attachments DROP FOREIGN KEY FK_D440DDE7AA8F9B32');
        $this->addSql('ALTER TABLE tbl_product_attachments ADD CONSTRAINT FK_D440DDE7AA8F9B32 FOREIGN KEY (job_kind_id) REFERENCES tbl_products (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tbl_product_attachments DROP FOREIGN KEY FK_D440DDE7AA8F9B32');
        $this->addSql('ALTER TABLE tbl_product_attachments ADD CONSTRAINT FK_D440DDE7AA8F9B32 FOREIGN KEY (job_kind_id) REFERENCES tbl_job_kinds (id)');
    }
}
